const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		//required: requires the data for our fields/properties to be included when creating a record
		//1st element defines if the field is required or not
		//2nd element is the message that will be printed out in our terminal when the data is not present
		required: [true, "Course Name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	//if nakahide sa users
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required : [true, "UserId is required."]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Course", courseSchema);