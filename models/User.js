const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Please enter your mobile number."]
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true]
			},
			enrolledOn: {
				type: Date,
				default: new Date(),
			},
			status: {
				type: String,
				default: "Enrolled" 
			}
		}
	]
})


module.exports = mongoose.model("User",userSchema);