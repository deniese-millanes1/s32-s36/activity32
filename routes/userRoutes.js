const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers")
const auth = require("../auth");

//Checking if an email already exist
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(
		resultFromController));
});


//User Registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));
})

//User Authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController));
})

//Activity s33
//Retrieve User Details
//The "auth.verify" will acts as a middleware to ensure that the user is logged in before they can get the details
router.get("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.userInformation({userId: userData.id}).then(resultFromController => res.send(
		resultFromController));
});


// enrolling
// router.post("/enroll", auth.verify, (req,res) => {
// 	let userData = {
// 		//userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}
// 	userController.enroll(data).then(resultFromController => res.send(
// 		resultFromController));
// })

router.post("/enroll", auth.verify, (req,res) => {
 	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	// if (data.isAdmin == true) {
	// 	return "you are not a user" 
	// } else { 
	userController.enroll(data).then(resultFromController => res.send(
		resultFromController));
	//}
})


module.exports = router; 