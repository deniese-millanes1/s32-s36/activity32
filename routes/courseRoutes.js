const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for create a course
 // router.post("/", (req, res) => {
	// courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
 // })

 router.post("/", auth.verify, (req,res) => {

	const adminData = auth.decode(req.headers.authorization);
 	console.log(adminData);
 	
 	if (adminData.isAdmin == true) {
	 	courseController.addCourse(req.body).then(resultFromController => res.send(
	 	resultFromController));
	 } else {
	 	return "Not an Admin";
	 }
 });
 
//Route fore retrieving all the courses
//nakalogin muna si user before retrieving all the courses
router.get("/all", auth.verify, (req,res) => {

 	courseController.getAllCourses().then(resultFromController => res.send(
 		resultFromController));
})

/* Mini Activity
	Create a route and controller for retrieving all active courses. No need to logged in
		functionName: getAllActive
		endpoint: "/"
*/

//Retrieve all active course
router.get("/", (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(
		resultFromController));
})

//Retrieve a specific course
router.get("/:courseId", (req,res) => {
	console.log(`course id: ${req.params.id}`)

	courseController.getCourse(req.params).then(resultFromController => res.send(
		resultFromController));
})

//Updating a course
router.put("/:courseId", auth.verify, (req,res) => {
	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}
	courseController.updateCourse(data).then(resultFromController => res.send(
		resultFromController));
})

//Archive
router.put("/:courseId/archive", auth.verify, (req,res) => {
	const archiveData = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(archiveData).then(resultFromController => res.send(
		resultFromController));
})
 

module.exports = router;

