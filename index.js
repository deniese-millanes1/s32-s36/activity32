const express = require("express");
const mongoose = require("mongoose");
//Allows our backend application to be avaiabe to our frontend application 
//Allow us to control the app's Cross Orgin Resource Sharing settings
const cors = require("cors");
const port = 4000;
const app = express();
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

//Connecting MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@cluster0.pjjaa.mongodb.net/s32-s36?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

//Checking connection if may error or none
let db = mongoose.connection;
//with error
db.on("error", () => console.error.bind(console,"Connection Error"));
//no error
db.once("open", () => console.log("Now connected to MongoDB Atlas"));

//Allows all resource to access our backend application
app.use(cors());
//json files
app.use(express.json());
//forms
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
//Will used the defined port number for the application whenver an environment variable is availale OR will use port 4000 if none is defined
//This syntax will allow flexibility when using the application locally or as a hosted application
//Ginagamit pag hinost na online ang process.env.PORT at kung hindi avaible ang isa sa kanila
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
});