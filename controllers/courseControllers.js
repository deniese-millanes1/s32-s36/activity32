const Course = require("../models/Course");

 module.exports.addCourse = (reqBody) => {
  	let newCourse = new Course ({
 		name: reqBody.name,
  		description: reqBody.description,
  		price: reqBody.price
  	})
 	return newCourse.save().then((course, error) => {
 		if(error) {
  			return false
 		} else {
 			return true		
 	}
	})
 }

//Retrieve all courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result
    })
}

module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
        return result
    })
}

//Retrieve specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    })
}

//Updating a course
module.exports.updateCourse = (data) => {
    console.log(data)
    return Course.findById(data.courseId).then((result, error) => {
        console.log(result);

        if (data.isAdmin) {
            result.name = data.updatedCourse.name
            result.description = data.updatedCourse.description
            result.price = data.updatedCourse.price
            console.log(result);
            return result.save().then((updatedCourse, error) => {
                if (error) {
                        return false
                } else {
                    return updatedCourse
                }
            })
        } else {
            return "Not an Admin"
        }
    })
}

//Archive
module.exports.archiveCourse = (archiveData) => {
    return Course.findById(archiveData.courseId).then((result, error) => {
        if (archiveData.isAdmin) {
            result.isActive = false 
        return result.save().then((archived,error) => {
            if (error) {
                return false
            } else {
                return true
            }
        })
        } else {
            return `Not Authorized To Do This Action. 
            If you are an admin, please login.`
        }
    })
}


