const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length>0) {
			return true
		} else {
			return false
		}
	})
}

//User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
	//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
	//hashSync(dataTobeHash,saltvalue)
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
	//compareSync(dataFromTheUserOrdataToBeCompared,encryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}

//Activity s33
//Retrieving User Details
module.exports.userInformation = (data) => {
	return User.findById(data.userId).then((result,error) => {
		if (error) {
			return ("No user have this id.")
		} else {
			result.password = " ";
			return (result);
		}
	})
}

//Controller for enrolling a user
module.exports.enroll = async (data) => {
	console.log(data)
	if (data.isAdmin == false) { 
	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user,error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});

		return course.save().then((course,error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	if (isUserUpdated && isCourseUpdated) {
		return `You are now enrolled at courseId: ${data.courseId}`
	} else {
		return false
	}
	} else {
		return "You are not a user"
	} 
}


// module.exports.enroll = async (userData) => {
// 	if (userData.isAdmin) {
// 		return "You are not a user"
// 	} else {
// 		let isUserUpdated = await User.findById(userData.userId).then(user => {
// 			user.enrollments.push({courseId: userData.courseId});

// 			return user.save().then((user,error) => {
// 				if (error) {
// 					return false
// 				} else {
// 					return true
// 				}
// 			})
// 		})

// 		let isCourseUpdated = await Course.findById(duserData.courseId).then(course => {
// 			course.enrollees.push({userId: userData.userId});

// 			return course.save().then((course,error) => {
// 				if (error) {
// 					return false
// 				} else {
// 					return true
// 				}
// 			})
// 		})

// 		if (isUserUpdated && isCourseUpdated) {
// 			return true
// 		} else {
// 			return false
// 		}
// 	}
// }